import { useEffect } from 'react'
import { useState } from 'react'

function App() {
  const [funnyBusiness, setFunnyBusiness] = useState("")

  useEffect( () => {
    initialize();
  }, []);

  const initialize = async () => {
    const url = process.env.NODE_ENV === "production" ? "./funnybusiness" : "http://localhost:3000/funnybusiness";
    const response =  await fetch(url);
    const fechedItem = await response.text();
    setFunnyBusiness(fechedItem);
    console.log("Initialized");
  }

  return (
    <div>
      <h1>moi vaan</h1>
      {funnyBusiness !== "" && <p>{funnyBusiness}</p>}
    </div>
  )
}

export default App
