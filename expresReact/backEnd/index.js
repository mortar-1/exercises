import express from "express";
import cors from "cors";


const server = express();
server.use(cors());
server.use("/", express.static("../frontEnd/dist"))

server.get("/funnybusiness", (req, res) => {
    res.send("This is funnybusiness!")
})

server.listen(3000);