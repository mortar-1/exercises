import { useEffect } from 'react';
import { useState } from 'react'
import { useLoaderData } from "react-router-dom"

export const loader = ({ params }) => {
  const id = Number(params.id)
  return id;
}

function Contact() {

  const id = useLoaderData();

  const [contacts, setContacts] = useState([])

  const initialContacts = [
    {
      id: 1,
      name: "Seppo Tiitinen",
      phone: "0405617895",
      email: "seppo.tiitinen@gmail.com"
    },
    {
      id: 2,
      name: "Laura Tiitinen",
      phone: "0405617895",
      email: "laura.tiitinen@gmail.com"
    },
    {
      id: 3,
      name: "Mikko Mallikas",
      phone: "0405617895",
      email: "mikko.mallikas@gmail.com"
    }
  ];

  useEffect(() => {
    setContacts(initialContacts);
  }, [])

  const contact = contacts.find(c => c.id === id);

  if (!contact) {
    return null;
  }

  return (
    <div>
      <h1>{contact.name}</h1>
      <p>{contact.phone}</p>
      <p>{contact.email}</p>
    </div>
  )
}

export default Contact 
