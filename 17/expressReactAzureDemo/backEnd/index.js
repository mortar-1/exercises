import express from "express";

const server = express();

server.use("/", express.static("./dist"))

const PORT = 3000;
server.listen(PORT, () => {
    console.log("listening to port", PORT);
})