import songList from "./songList";
import { Link, Outlet } from "react-router-dom";
import axios from "axios";
import { useState } from "react";
import { useEffect } from "react";

const Songs = () => {

  const SongListDev = () => {
    return (
      <div>
        <ul>
          {songList.map((s, i) => <Link to={`/${s.id}`} key={i + s.title}>{s.title}</Link>)}
        </ul>
      </div>
    )
  }

  if (process.env.NODE_ENV === "development") {
    return (
      <div>
        <SongListDev />
        <Outlet />
      </div>
    )
  } else {
    
    const [list, setList] = useState(null);
    useEffect(() => {
      const getData = async () => {
        const response = await axios.get("./songs");
        const newList = await response.data;
        setList(newList);
      }
      getData();
    }, [])

    const SongListProd = () => {
      if (list === null) {
        return null;
      }
      return (
        <div>
          <ul>
            {list.map((s, i) => <Link to={`/${s.id}`} key={i + s.title}>{s.title}</Link>)}
          </ul>
        </div>
      )
    }
    return (
      <div>
        <SongListProd />
        <Outlet />
      </div>
    )
  }
}

export default Songs
