CREATE TABLE "songs" (
 "id" SERIAL PRIMARY KEY,
 "title" varchar NOT NULL,
 "lyrics" varchar NOT NULL
 );

INSERT INTO songs (title, lyrics)
VALUES ('Song 1', 'Lyrics to song 1');

... 5 kertaa edellinen ...
