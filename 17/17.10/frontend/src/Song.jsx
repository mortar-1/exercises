import { useLoaderData } from "react-router-dom";
import axios from "axios";

const throw404Error = () => {
    const error = new Error('404 - Not Found');
    error.status = 404;
    throw error;
}

const getData = async (id) => {
    try {
        const response = await axios.get(`./songs/${id}`)
        const song = await response.data;
        return song;
    } catch {
        throw404Error();
    }
}

export const loader = ({ params }) => {
    const id = Number(params.id);
    const song = getData(id);
    return song;

}

const Song = () => {
    const song = useLoaderData();
    return (
        <div>
            <h1>{song.title}</h1>
            <p>{song.lyrics}</p>
        </div>
    )
}

export default Song;