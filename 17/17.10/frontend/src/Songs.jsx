import { Link, Outlet } from "react-router-dom";
import axios from "axios";
import { useState } from "react";
import { useEffect } from "react";

const Songs = () => {

  const [list, setList] = useState(null);

  useEffect(() => {
    getData();
  }, []);

  const getData = async () => {
    const response = await axios.get("./songs");
    const newList = response.data;
    setList(newList);
  }

  const SongList = () => {
    if (list === null) {
      return null;
    }
    return (
      <div>
        <ul>
          {list.map((s, i) => <Link to={`/${s.id}`} key={i + s.title}>{s.title}</Link>)}
        </ul>
      </div>
    )
  }

  return (
    <div>
      <SongList />
      <Outlet />
    </div>
  )
}


export default Songs
