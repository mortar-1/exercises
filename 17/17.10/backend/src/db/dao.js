import executeQuery from "./db.js"
import queries from "./queries.js"

const findAll = async () => {
    console.log("requesting for all songs...", queries.findAll);
    const result = await executeQuery(queries.findAll);
    console.log(`Found ${result.rows.length} songs`);
    return result;
}

const findOne = async (id) => {
    console.log("requesting for one song with id", id);
    const result = await executeQuery(queries.findOne, [id]);
    console.log(`Found ${result.rows.length} songs`);
    return result;
}

const addSong = async (title, lyrics) => {
    console.log(`Adding one song. Title: ${title}, Lyrics: ${lyrics}`);
    const result = await executeQuery(queries.addOne, [title, lyrics]);
    return result;
}

const deleteSong = async (id) => {
    console.log("Deleting song with id:", id);
    const result = await executeQuery(queries.deleteOne, [id]);
    return result;
}

const updateSong = async (title, lyrics, id) => {
    console.log("Updating song with id:", id);
    const result = await executeQuery(queries.updateOne, [title, lyrics, id]);
    return result;
}

export default { findAll, findOne, addSong, deleteSong, updateSong }