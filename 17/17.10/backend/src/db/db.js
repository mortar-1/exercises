import pg from "pg";

const isProd = process.env.NODE_ENV === "production"

const HOST = isProd
    ? "172.17.0.2"
    : "localhost"

export const pool = new pg.Pool({
    host: HOST,
    port: 5432,
    user: "pguser",
    password: "pgpass",
    database: "song-book",
    ssl: false
});

export const executeQuery = async (query, parameters) => {
    const client = await pool.connect();
    try {
        const result = await client.query(query, parameters);
        return result;
    } catch (error) {
        console.error(error.stack);
        throw error;
    } finally {
        client.release();
    }
}

export default executeQuery