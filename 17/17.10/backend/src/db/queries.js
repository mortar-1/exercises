const findAll = `
SELECT * FROM songs
`

const findOne = `
SELECT * FROM songs WHERE id = $1
`

const addOne = `
INSERT INTO songs (title, lyrics) VALUES ($1, $2)
`

const deleteOne = `
DELETE FROM songs WHERE id = $1
`

const updateOne = `
UPDATE songs
SET title = $1, lyrics = $2
WHERE id = $3
`

export default { findAll, findOne, addOne, deleteOne, updateOne }
