import express from "express";
import path from "path";
import url from "url";
import cors from "cors";

import dao from "./db/dao.js";

const app = express();

app.use(express.json());

if (process.env.NODE_ENV !== "development") {
    app.use(cors());
}

const currentDirectory = path.dirname(url.fileURLToPath(import.meta.url))
const distDirectory = path.resolve(currentDirectory, '../dist/')

app.get("/songs", async (req, res) => {
    const result = await dao.findAll();
    res.status(201).send(result.rows);
})

app.get("/songs/:id", async (req, res) => {
    const id = Number(req.params.id);
    const result = await dao.findOne(id);
    const song = result.rows[0];
    if (song === undefined) {
        res.status(404).send({ error: "Song not found" })
    };
    res.status(201).json(song);
})

app.delete("/songs/:id", async (req, res) => {
    const id = Number(req.params.id);
    await dao.deleteSong(id);
    res.status(204).end();
})

app.post("/songs", async (req, res) => {
    const title = req.body.title;
    const lyrics = req.body.lyrics;
    if (!title || !lyrics) {
        return res.status(400).send({ error: "title and lyrics required" });
    }
    console.log("Adding song...");
    await dao.addSong(title, lyrics);
    res.status(200).send({ Message: "New song created" });
})

app.put("/songs/:id", async (req, res) => {
    const id = Number(req.params.id);
    const findOneResult = await dao.findOne(id);
    const oldSong = findOneResult.rows[0];
    if (!oldSong) {
        return res.status(400).send({ error: "no song with id:", id });
    }
    const title = req.body.title ? req.body.title : oldSong.title;
    const lyrics = req.body.lyrics ? req.body.lyrics : oldSong.lyrics;
    await dao.updateSong(title, lyrics, id);
    res.status(200).send({ Message: "Song updated" });
})

app.use(express.static(distDirectory));

app.use((req, res) => {
    res.sendFile('index.html', { root: distDirectory })
})

const PORT = 3000;
app.listen(PORT, () => {
    console.log("listening to port", PORT);
})