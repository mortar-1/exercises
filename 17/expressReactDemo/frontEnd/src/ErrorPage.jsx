import { useRouteError, Link } from "react-router-dom";

const ErrorPage = () => {
    const error = useRouteError();

    if (error.status === 404) return (
        <div>
            <h1>404 - not found</h1>
            <p>The requested page is not here</p>
            <Link to={"/admin"}>Admin page</Link>
        </div>
    )

    console.error(error);
    return (
        <div>
            <h1>An Unexpected error happened!</h1>
            <p>
                {error.statusText || error.message}
            </p>
        </div>
    )
}

export default ErrorPage;