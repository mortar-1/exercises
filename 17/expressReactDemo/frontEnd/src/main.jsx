import React from 'react'
import ReactDOM from 'react-dom/client'
import Admin from './Admin'
import Contact, { loader as contactLoader } from './Contact'
import Navigator from './Navigator'
import ErrorPage from './ErrorPage'


import { createBrowserRouter, RouterProvider } from "react-router-dom"

const router = createBrowserRouter([
  {
    path: "/",
    errorElement: <ErrorPage />
  },
  {
    path: "/contact",
    element: <Navigator />,
    children: [
      {
        path: ":id",
        element: <Contact />,
        loader: contactLoader
      }
    ]
  },
  {
    path: "/admin",
    element: <Admin />
  }
])



ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
