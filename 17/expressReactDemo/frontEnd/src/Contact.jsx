import { useEffect } from 'react';
import { useState } from 'react'
import { useLoaderData } from "react-router-dom"
import initialContacts from "./initialContacts"

export const loader = ({ params }) => {
  const id = Number(params.id)
  return id;
}

function Contact() {

  const id = useLoaderData();

  const [contacts, setContacts] = useState([]) 

  useEffect(() => {
    setContacts(initialContacts);
  }, [])

  const contact = contacts.find(c => c.id === id);

  if (!contact) {
    return null;
  }

  return (
    <div>
      <h1>{contact.name}</h1>
      <p>{contact.phone}</p>
      <p>{contact.email}</p>
    </div>
  )
}

export default Contact 
