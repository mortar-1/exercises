import express from "express"
import url from "url"
import path from "path"

const server = express();

const currentDirectory = path.dirname(url.fileURLToPath(import.meta.url));
const distDirectory = path.resolve(currentDirectory, '../frontEnd/dist/');

server.use("/", express.static(distDirectory));

server.use( (req,res) => {
    res.sendFile("index.html", {root: distDirectory});
})

const PORT = 3000;
server.listen(PORT, () => {
    console.log("Listening to port", PORT);
});