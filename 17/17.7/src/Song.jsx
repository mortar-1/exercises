import { useLoaderData } from "react-router-dom";
import songList from "./songList";

const throw404Error = () => {
    const error = new Error('404 - Not Found');
    error.status = 404;
    throw error;
}

export const loader = ({ params }) => {
    const id = Number(params.id);
    const song = songList.find(s => s.id === id);
    if (song === undefined) {
        throw404Error()
    }
    return song;
}

const Song = () => {
    const song = useLoaderData();

    return (
        <div>
            <h1>{song.title}</h1>
            <p>{song.lyrics}</p>
        </div>
    )
}

export default Song;