import songList from "./songList";
import { Link, Outlet } from "react-router-dom";

const Songs = () => {

  const SongList = () => {
    return (
      <div>
        <ul>
          {songList.map((s, i) => <Link to={`/${s.id}`} key={i + s.title}>{s.title}</Link>)}
        </ul>
      </div>
    )
  }

  return (
    <div>
      <SongList />
      <Outlet />
    </div>
  )
}

export default Songs
