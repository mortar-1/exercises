const initialContacts = [
    {
      id: 1,
      name: "Seppo Tiitinen",
      phone: "0405617895",
      email: "seppo.tiitinen@gmail.com"
    },
    {
      id: 2,
      name: "Laura Tiitinen",
      phone: "0405617895",
      email: "laura.tiitinen@gmail.com"
    },
    {
      id: 3,
      name: "Mikko Mallikas",
      phone: "0405617895",
      email: "mikko.mallikas@gmail.com"
    }
  ];

  export default initialContacts;