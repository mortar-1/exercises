import { Link, Outlet } from "react-router-dom";
import Contact from "./Contact"
import initialContacts from "./initialContacts"

const Navigator = () => {

    const contacts = initialContacts;

    const Links = () => {
        return (
            <nav>
                {contacts.map((c, i) => <Link key={i + c.name} to={c.id.toString()}>{c.name}</Link>)}
            </nav>
        )
    }

    return (
        <div>
            <Links />
            <Outlet />
        </div>
    )
}

export default Navigator;