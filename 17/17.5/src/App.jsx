import { useState } from 'react'

function App() {
  const [color, setColor] = useState("#f5f9a2")

  const randomColor = () => {
    const randomColor = Math.floor(Math.random() * 16777215).toString(16);
    setColor("#" + randomColor);
  }

  const handleClick = () => {
    randomColor();
  }

  return (
    <div style={{border: "5px solid", display: "flex", flexDirection: "column", alignItems: "center"}}>
      <p>{color}</p>
      <button style={{backgroundColor: color, height: 200, width: 200}}
      onClick={handleClick}></button>
    </div>
  )
}

export default App
