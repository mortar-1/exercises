import React from 'react'
import ReactDOM from 'react-dom/client'
import Admin from './Admin'
import Contact from './Contact'

import { createBrowserRouter, RouterProvider } from "react-router-dom"

const router = createBrowserRouter([
  {
    path: "/contacts",
    element: <Contact />
  },
  {
    path: "/admin",
    element: <Admin />
  }
])



ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
