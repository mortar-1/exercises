import express from "express";
import songList from "./songList.js";
import path from "path";
import url from "url";
import cors from "cors";

const server = express();

server.use(express.json());
if (process.env.NODE_ENV !== "development") {
    server.use(cors());
}

const currentDirectory = path.dirname(url.fileURLToPath(import.meta.url))
const distDirectory = path.resolve(currentDirectory, '../dist/')

server.get("/songs", (req, res) => {
    res.json(songList)
})

server.get("/songs/:id", (req, res) => {
    const id = Number(req.params.id);
    const song = songList.find(s => s.id === id);
    if (song === undefined) {
        res.status(404).send({ error: "Song not found" })
    };
    res.status(201).json(song);
})

server.use(express.static(distDirectory));

server.use((req, res) => {
    res.sendFile('index.html', { root: distDirectory })
})

const PORT = 3000;
server.listen(PORT, () => {
    console.log("listening to port", PORT);
})