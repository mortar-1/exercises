import { useLoaderData } from "react-router-dom";
import songList from "./songList";
import axios from "axios";
import { useState } from "react";

const throw404Error = () => {
    const error = new Error('404 - Not Found');
    error.status = 404;
    throw error;
}

export const loader = ({ params }) => {
    const id = Number(params.id);
    if (process.env.NODE_ENV === "development") {
        const song = songList.find(s => s.id === id);
        if (song === undefined) {
            throw404Error()
        }
        return song;
    } else {
        const getData = async () => {
            try {
                const response = await axios.get(`http://localhost:3000/songs/${id}`)
                const song = await response.data;
                return song;
            } catch {
                throw404Error();
            }
        }
        return getData();
    }
}

const Song = () => {

    const song = useLoaderData();
 
    return (
        <div>
            <h1>{song.title}</h1>
            <p>{song.lyrics}</p>
        </div>
    )
}

export default Song;