import React from "react"
import ReactDOM from "react-dom/client"
import Songs from "./songs"
import Song, { loader as songLoader } from "./Song"
import ErrorPage from "./ErrorPage"
import { createBrowserRouter, RouterProvider } from "react-router-dom";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Songs />,
    errorElement: <ErrorPage />,
    children: [
      {
        path: ":id",
        element: <Song />,
        loader: songLoader
      }
    ]
  },
])

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
)
