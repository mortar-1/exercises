import pg from "pg";
import * as queries from "./queries";

export const pool = new pg.Pool({
    host: "jsc-lecture-17-demo.postgres.database.azure.com",
    port: 5432,
    user: "jscadmin@jsc-lecture-17-demo",
    password: "jscpass1!",
    database: "jsc-lecture-17-demo",
    ssl: true
});

export const executeQuery = async (query, parameters) => {
    const client = await pool.connect();
    try{
        const result = await client.query(query, parameters);
        return result;
    } catch (error){
        console.error(error.stack);
        throw error;
    } finally {
        client.release();
    }
}

export default executeQuery