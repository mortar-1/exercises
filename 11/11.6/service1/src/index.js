import express from "express"

const server = express();

server.get("/", (req, res) => {
    res.send("service1 working")
});

server.listen(3001, () => {
    console.log("listening to port 3001");
})