import exp from "express"

const server = exp();

server.get("/", (req, res) => {
    res.send("Docker + Azure  :D")
})

const PORT  = process.env.PORT
server.listen(PORT || 3000, () => {
    console.log("Server listening to port", PORT || 3000);
})