module.exports = async function (context, req) {
    context.log('Function triggered.')

    const input = req.body?.input;

    let status, body;

    if (!input) {
        status = 400;
        body = "Missing input 'input'";
    } else {
        status = 200;
        body = input.toUpperCase();
    }

    context.res = ({ status, body })
}