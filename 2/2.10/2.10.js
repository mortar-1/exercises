const students = [
    { name: "Markku", score: 99 },
    { name: "Karoliina", score: 58 },
    { name: "Susanna", score: 69 },
    { name: "Benjamin", score: 77 },
    { name: "Isak", score: 49 },
    { name: "Liisa", score: 89 },
];

// 1

let highestScoring = students[0];
let lowestScoring = students[0];
for (const student of students) {
    if (student.score > highestScoring.score) {
        highestScoring = student;
    }
    if (student.score < lowestScoring.score) {
        lowestScoring = student;
    }
}
console.log("Highest scoring student:", highestScoring);
console.log("Lowest scoring student:", lowestScoring);

// 2

let sum = 0;

for (const student of students) {
    sum += student.score;
}

const average = sum / students.length;

console.log("Average score:", average);

// 3

let studentsWithHigherThanAverage = [];

for (const student of students) {
    if (student.score > average) {
        studentsWithHigherThanAverage.push(student);
    }
}

console.log("Students with higher than average score:", studentsWithHigherThanAverage);

// 4 

for (const student of students) {
    if (student.score < 1) {
        student.grade = 0;
    }
    if (student.score >= 1) {
        student.grade = 1;
    }
    if (student.score >= 40) {
        student.grade = 2;
    }
    if (student.score >= 60) {
        student.grade = 3;
    }
    if (student.score >= 80) {
        student.grade = 4;
    }
    if (student.score >= 95) {
        student.grade = 5;
    }
}

console.log("With grades:", students);