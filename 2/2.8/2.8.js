const rows = process.argv[2];

let row = "";

/*      ** LEVEL 1 FOR **
for (let i = 1; i <= rows; i++) {
    row += "&";
    console.log(row);   
}
*/

/*      ** LEVEL 1 WHILE **
let i = 1;
while (i <= rows) {
    row += "&";
    console.log(row);
    i++;
}
*/

let emptySpacesAtBeginning = rows - 1;

/*      ** LEVEL 2 FOR **
for (let i = 1; i <= emptySpacesAtBeginning; i++) {
    row += " ";
}
row += "&";

console.log(row);

for (let i = 2; i <= rows; i++) {
    row += "&&";
    row = row.substring(1);
    console.log(row);
}
*/

//      ** LEVEL 2 WHILE **

let i = 1;  
while (i <= emptySpacesAtBeginning) {
    row += " ";
    i++;
}
row += "&";
console.log(row);

i = 2;
while (i <= rows) {
    row += "&&";
    row = row.substring(1);
    console.log(row);
    i++;
}

