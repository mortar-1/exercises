// eslint-disable-next-line no-unused-vars
const [a, b, ...rest] = process.argv;

let number1 = { name: "number1" };
let number2 = { name: "number2" };
let number3 = { name: "number3" };
const numbers = [number1, number2, number3];
let largest = numbers[0];
let smallest = numbers[0];

for (let i = 0; i < rest.length; i++) {
    numbers[i].value = parseInt(rest[i]);
}
for (const number of numbers) {
    if (number.value > largest.value) {
        largest = number;
    }
    if (number.value < smallest.value) {
        smallest = number;
    }
}

if (smallest.value === largest.value) {
    console.log("They are all equal");
} else {
    console.log(`Largest number is '${largest.name}' with the value of ${largest.value}`);
    console.log(`Smallest number is '${smallest.name}' with the value of ${smallest.value}`);
}