const a = parseInt(process.argv[2]);
const b = parseInt(process.argv[3]);
const c = process.argv[4];

if (a === b && c === "hello world") {
    console.log("yay, you guessed the password");
} else if (a === b && a !== undefined) {
    console.log("they are equal");
} else if (a > b) {
    console.log("a is greater");
} else if (b > a) {
    console.log("b is greater");
} else {
    console.log("...?");
}