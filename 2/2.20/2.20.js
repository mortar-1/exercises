// eslint-disable-next-line no-unused-vars
const [a, b, char1, char2, str] = process.argv;

//      ** 1 **

let lastSpaceIndex = 0;
for(let i = 0; i < str.length; i++) {
    if (str[i] === " ") {
        lastSpaceIndex = i;
    }
}

console.log("Last word cut out:", str.substring(0,lastSpaceIndex));

//      ** 2 **

console.log(str.replace(new RegExp(char1, "g"), char2));