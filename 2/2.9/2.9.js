const arr = [1, 4, 6, 32, 25, 16, 31, 15, 10, 2, 7]

let largest = Number.NEGATIVE_INFINITY;

for (let i = 0; i < arr.length; i++) {
    if (arr[i] > largest) {
        largest = arr[i];
    }
}

console.log("Largest:", largest);

let secondLargest = Number.NEGATIVE_INFINITY;

for (let i = 0; i < arr.length; i++) {
    if (arr[i] > secondLargest && arr[i] < largest) {
        secondLargest = arr[i];
    }
}

console.log("Second largest:", secondLargest);