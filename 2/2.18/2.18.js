// eslint-disable-next-line no-unused-vars
const [a,b, ...names] = process.argv;

//      ** 1 **
let str = "";
for (let i = 0; i < names.length; i++) {
    str += names[i].substring(0,1);
    if (i < names.length - 1) {
        str += ".";
    }
}


//      ** 2 **

console.log("name initials:", str);

str = "";
let longest = names[0];
let shortest = names[0];

for (const name of names) {
    if (name.length > longest.length) {
        longest = name;
    }
    if (name.length < shortest.length) {
        shortest = name;
    }
}

for (const name of names) {
    if (name !== longest && name !== shortest) {
        console.log(longest, name, shortest);
    }
}







