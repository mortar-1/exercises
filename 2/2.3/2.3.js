const arr = ["banaani", "omena", "mandariini", "appelsiini", "kurkku", "tomaatti", "peruna"];

console.log("3rd elemet", arr[2]);
console.log("5th element", arr[4]);
console.log("array length", arr.length);

arr.sort();

console.log(arr);

arr.splice(6, 0, "sipuli");

console.log(arr);

arr.shift();

console.log(arr);

arr.forEach(e => console.log(e));

arr.forEach(e => {
    if (e.includes("r")) {
        console.log(e);
    }
});