const month = parseInt(process.argv[2]);

if (month > 0 && month < 13) {
    if ((month % 2 !== 0 && month < 8)
        ||
        (month % 2 === 0 && month >= 8)
    ) {
        console.log("31 days");
    } else if (month === 2) {
        console.log("28 or 29 days");
    } else {
        console.log("30 days");
    }
} else {
    console.log("not a month");
}

