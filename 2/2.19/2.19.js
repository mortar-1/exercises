const lower = process.argv[2] === "lower";
const str = process.argv[3];

if (lower) {
    console.log(str.toLowerCase());
} else {
    console.log(str.toUpperCase());
}