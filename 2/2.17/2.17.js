const balance = parseInt(process.argv[2]);
const isActive = process.argv[3] === "true";
const checkBalance = process.argv[4] === "true";

console.log(isActive);
console.log(checkBalance);

if (checkBalance) {
    if (isActive && balance > 0) {
        console.log("Balance:", balance);
    } else {
        if (!isActive) {
            console.log("Your account is not active");
        } else {
            if (balance === 0) {
                console.log("Your account is empty");
            } else {
                console.log("Your balance is negative");
            }
        }

    }
} else {
    console.log("Have a nice day!");
}

