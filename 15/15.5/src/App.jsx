import { useState } from 'react'

function App() {

  const [num1, setNum1] = useState(0);
  const [num2, setNum2] = useState(0);
  const [num3, setNum3] = useState(0);


  const Button = ({ number, setFunction }) => {
    const plusOne = () => {
      setFunction(number + 1);
    }
    return (
      <div>
        <button onClick={plusOne}>
          {number}
        </button>
      </div>
    )
  }

  return (
    <div>
      <Button number={num1} setFunction={setNum1} />
      <Button number={num2} setFunction={setNum2} />
      <Button number={num3} setFunction={setNum3} />
      {num1 + num2 + num3}
    </div>
  )
}

export default App
