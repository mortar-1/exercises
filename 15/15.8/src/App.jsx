import { useState } from 'react'
import "./style.css"

function App() {

  const createSquares = () => {
    let squares = []
    for (let i = 1; i < 10; i++) {
      squares.push({ id: i, innerHTML: "", isMarked: false });
    }
    return squares;
  }

  const [player, setPlayer] = useState("X")
  const [squares, setSquares] = useState(createSquares)
  const [gameOver, setGameOver] = useState(false);
  const [winner, setWinner] = useState(null);

  const gameIsOver = () => {
    for (const square of squares) {
      if (square.innerHTML === "") {
        return false;
      }
    }
    return true;
  }

  const someoneWon = () => {
    const possibility1 = [squares[0], squares[1], squares[2]];
    const possibility2 = [squares[3], squares[4], squares[5]];
    const possibility3 = [squares[6], squares[7], squares[8]];

    const possibility4 = [squares[0], squares[3], squares[6]];
    const possibility5 = [squares[1], squares[4], squares[7]];
    const possibility6 = [squares[2], squares[5], squares[8]];

    const possibility7 = [squares[0], squares[4], squares[8]];
    const possibility8 = [squares[6], squares[4], squares[2]];

    const checkForWinner = (line) => {
      if (line[0].innerHTML === line[1].innerHTML && line[0].innerHTML === line[2].innerHTML && line[0].isMarked) {
        setWinner(line[0].innerHTML);
        return true;
      }
    }
    checkForWinner(possibility1);
    checkForWinner(possibility2);
    checkForWinner(possibility3);
    checkForWinner(possibility4);
    checkForWinner(possibility5);
    checkForWinner(possibility6);
    checkForWinner(possibility7);
    checkForWinner(possibility8);
  }

  const handleClick = (event) => {
    if (!winner) {
      const id = Number(event.target.id);
      const newSquaresList = squares.map(square => {
        if (square.id === id && square.innerHTML === "") {
          const newSquare = { id: id, innerHTML: player, isMarked: true };
          setPlayer(player === "X" ? "O" : "X");
          return newSquare;
        } else {
          return square;
        }
      })
      setSquares(newSquaresList);
    }
    setGameOver(gameIsOver);
    someoneWon();
  }

  const Squares = () => {
    return (
      squares.map(square =>
        <button key={"square" + square.id} onClick={handleClick} className="button" id={square.id}>{square.innerHTML}</button>
      ))
  };

  return (
    <div>
      <div className="grid">
        <Squares />
      </div>
      {(!gameOver && !winner) && <h1>Player: {player}</h1>}
      {(gameOver && !winner) && <h1>Game over</h1>}
      {winner && <h1>{winner} has won</h1>}
    </div>
  )
}

export default App
