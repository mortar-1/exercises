import { useState } from 'react'

const HeaderComponent = ({ text }) => {
  return (
    <h1>{text}</h1>
  )
}

const YearComponent = (props) => {
  return (
    <h2>It is year {new Date().getFullYear()}</h2>
  )
}

function App() {
  return (
    <div>
      <HeaderComponent text="Hello React!" />
      <YearComponent />
    </div>
  )
}

export default App
