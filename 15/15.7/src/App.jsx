import { useState } from 'react'

function App() {
  const [input, setInput] = useState("");
  const [toDoList, setToDoList] = useState([]);

  const handleInputChange = (event) => setInput(event.target.value)

  const handleButtonClick = () => {
    let id = input + Math.floor(Math.random() * 90000) + 10000;
    setToDoList(toDoList.concat({ input, id: id }));
    setInput("");
  }

  const handleItemClick = (event) => {
    const itemStyle = event.target.style;
    if (itemStyle.color !== "gray") {
      itemStyle.textDecoration = "line-through";
      itemStyle.color = "gray";
    } else {
      itemStyle.removeProperty("text-decoration");
      itemStyle.removeProperty("color");
    }
  }
  const handleRemoveButtonClick = (event) => {
    const id = event.target.id;
    const newTodoList = toDoList.filter(item => item.id !== id)
    setToDoList(newTodoList);
  }

  const RenderList = () => {
    const list = toDoList.map((item) => {
      return (
        <div>
          <li key={item.id} id={item.id} onClick={handleItemClick}>{item.input}</li>
          <button id={item.id} onClick={handleRemoveButtonClick}>Remove</button>
        </div>
      )
    });
    return (
      <ul>
        {list}
      </ul>
    )
  }

  return (
    <div>
      <h1>ToDoList</h1>
      <p>Click on item to make it done/un-done</p>
      <input value={input} onChange={handleInputChange} />
      <button onClick={handleButtonClick}>Add</button>
      <RenderList />
    </div>
  )
}

export default App
