import { useState } from 'react'

function App() {
  const [inputString, setInputString] = useState("");
  const [headerString, setHeaderString] = useState("");

  const onStringChange = (event) => setInputString(event.target.value);
  
  const onButtonClick = () => setHeaderString(inputString);

  return (
    <div>
      <h1>Your string is: {headerString}</h1>
      <input id="input" onChange={onStringChange} value={inputString}></input>
      <button onClick={onButtonClick}>Submit</button>
    </div>
  )
}

export default App
