import { useState } from 'react'

const namelist = ["Ari", "Jari", "Kari", "Sari", "Mari", "Sakari", "Jouko"];

const BoldNameElement = ({ name }) => {
  return (
    <div>
      <b>{name}</b>
    </div>
  )
}

const CursiveElement = ({ name }) => {
  return (
    <div>
      <i>{name}</i>
      <br />
    </div>
  )
}

const NamelistElement = ({ names }) => {
  const personDOMs = names.map((person, i) => {
    let key = person + i;
    if (i % 2 === 0) {
      return <BoldNameElement key={key} name={person} />
    } else {
      return <CursiveElement key={key} name={person} />
    }
  })
  return <div>{personDOMs}</div>
}

function App() {

  return (
    <div>
      <NamelistElement names={namelist} />
    </div>
  )

}

export default App
