import React, { useState, useEffect } from 'react'

function App() {
  const [input, setInput] = useState("");
  const ref = React.useRef(null);

  const handleReload = () => {
    setInput(ref.current.value);
    ref.current.value = "";
  }

  const CatPic = ({ says }) => {
    const src = says ? `https://cataas.com/c/s/${says}` : "https://cataas.com/c";
    return (
      <img src={src} alt="cat image" height="400" />
    )
  }

  return (
    <div>
      <CatPic says={input} />
      <input ref={ref} type="text"></input>
      <button onClick={handleReload}>reload</button>
    </div>
  )
}

export default App
