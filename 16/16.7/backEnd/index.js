import express from "express";
import cors from "cors"

const server = express();
server.use(cors());
server.use("/", express.static("../frontEnd/dist"));

server.get("/:number", (req, res) => {
    const num = req.params.number;
    if (num === "2412") {
        res.send("Merry Christmas!")
    } else {
        res.send(num);
    }
})

const PORT = 3000;
server.listen(PORT, () => {
    console.log(`Server running on port: ${PORT}`);
})