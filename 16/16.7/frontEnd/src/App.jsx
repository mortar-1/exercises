import { useState } from "react";
import axios from "axios";
import { useEffect } from "react";

function App() {
  const [input, setInput] = useState("");
  const [firstNum, setFirstNum] = useState(0);
  const [secondNum, setSecondNum] = useState(0);
  const [operator, setOperator] = useState("");
  const [result, setResult] = useState(0);
  const [testInput, setTestInput] = useState("");

  const url = process.env.NODE_ENV === "production" ? "./" : "http://localhost:3000/";

  useEffect(() => {
    if (operator === "") {
      return;
    }

    switch (operator) {
      case "+":
        add();
        break;
      case "-":
        substract();
        break;
      case "*":
        multiply();
        break;
      case "/":
        divide();
        break;
    }
  }, [secondNum]);

  const handleNumberButton = (event) => {
    const value = event.target.value;
    if (value === "." && input.includes(".")) {
      return;
    }
    setInput(input + value);
  }

  const add = async () => {
    const result = firstNum + secondNum;
    setResult(result);
    const response = await axios.get(url + result);
    setOperator("");
    setFirstNum(0);
    setSecondNum(0);
    setInput(response.data);
  }

  const substract = async () => {
    const result = firstNum - secondNum;
    setResult(result);
    const response = await axios.get(url + result);
    setOperator("");
    setFirstNum(0);
    setSecondNum(0);
    setInput(response.data);
  }

  const multiply = async () => {
    const result = firstNum * secondNum;
    setResult(result);
    const response = await axios.get(url + result);
    setOperator("");
    setFirstNum(0);
    setSecondNum(0);
    setInput(response.data);
  }

  const divide = async () => {
    const result = firstNum / secondNum;
    setResult(result);
    const response = await axios.get(url + result);
    setOperator("");
    setFirstNum(0);
    setSecondNum(0);
    setInput(response.data);
  }

  const handleOperatorButton = (event) => {
    const value = event.target.value;
    setFirstNum(Number(input));
    clearScreen();
    setOperator(value);
  }

  const handleResultButton = () => {
    setSecondNum(Number(input));
  }

  const clearScreen = () => {
    setInput("");
  }

  const Calculator = () => {
    return (
      <table className="calculator" >
        <tbody>
          <tr>
            <td colSpan="3"> <input className="display-box" type="text" id="result" value={input} disabled /> </td>
            <td> <input type="button" value="C" id="btn" onClick={clearScreen} /> </td>
          </tr>
          <tr>
            <td> <input type="button" value="1" onClick={handleNumberButton} /> </td>
            <td> <input type="button" value="2" onClick={handleNumberButton} /> </td>
            <td> <input type="button" value="3" onClick={handleNumberButton} /> </td>
            <td> <input type="button" value="/" onClick={handleOperatorButton} /> </td>
          </tr>
          <tr>
            <td> <input type="button" value="4" onClick={handleNumberButton} /> </td>
            <td> <input type="button" value="5" onClick={handleNumberButton} /> </td>
            <td> <input type="button" value="6" onClick={handleNumberButton} /> </td>
            <td> <input type="button" value="-" onClick={handleOperatorButton} /> </td>
          </tr>
          <tr>
            <td> <input type="button" value="7" onClick={handleNumberButton} /> </td>
            <td> <input type="button" value="8" onClick={handleNumberButton} /> </td>
            <td> <input type="button" value="9" onClick={handleNumberButton} /> </td>
            <td> <input type="button" value="+" onClick={handleOperatorButton} /> </td>
          </tr>
          <tr>
            <td> <input type="button" value="." onClick={handleNumberButton} /> </td>
            <td> <input type="button" value="0" onClick={handleNumberButton} /> </td>


            <td> <input type="button" value="=" id="btn" onClick={handleResultButton} /> </td>
            <td> <input type="button" value="*" onClick={handleOperatorButton} /> </td>
          </tr>
        </tbody>
      </table>
    )
  }

  const handleTestInput = (event) => setTestInput(event.target.value);

  const Input = ({ name, value, handler }) => {
    return (
      <div>
        <label htmlFor={name}>{name}</label>
        <input type="text" id={name} onChange={handler} value={value} />
      </div>
    )
  }

  return (
    <div>
      <Calculator />
      <h1>Result: {result}</h1>
      {Input (name="testInput", value={testInput}, handler={handleTestInput})}
    </div>
  )
}

export default App
