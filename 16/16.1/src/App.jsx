import { useState } from 'react'

function App() {
  const [input, setInput] = useState("");
  const [toDoList, setToDoList] = useState([]);

  const handleInputChange = (event) => setInput(event.target.value)

  const handleButtonClick = () => {
    let id = input + Math.floor(Math.random() * 90000) + 10000;
    setToDoList(toDoList.concat({ input, id: id }));
    setInput("");
  }

  const handleItemClick = (event) => {
    console.log("toDoList:", toDoList);
    const id = event.target.id;
    console.log("id:", id)
    const newTodoList = toDoList.filter(item => item.id !== id)
    setToDoList(newTodoList);
  }

  const generateListItem = (input) => {
        return <li key={key} id={key} onClick={handleItemClick}>{input}</li>
  }

  const RenderList = () => {
    const list = toDoList.map((item) => {
      return <li key={item.id} id={item.id} onClick={handleItemClick}>{item.input}</li>
    });
    return (
      <ul>
        {list}
      </ul>
    )
  }

  return (
    <div>
      <input value={input} onChange={handleInputChange} />
      <button onClick={handleButtonClick}>Add</button>
      <RenderList />
    </div>
  )
}

export default App
