import { useState } from 'react'
import './App.css'

function App() {
  const [search, setSearch] = useState("");
  const [contacts, setContacts] = useState([]);
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [address, setAddress] = useState("");
  const [website, setWebsite] = useState("");
  const [info, setInfo] = useState("");
  const [inAddingMode, setInAddingMode] = useState(false);
  const [inEditingMode, setInEditingMode] = useState(false);
  const [inViewContactMode, setViewContactMode] = useState(false);
  const emptyContact = { name: "", email: "", phone: "", address: "", website: "", info: "" };
  const [contact, setContact] = useState(emptyContact)

  const generateRandomId = () => {
    const newId = Math.ceil(Math.random() * 10000000);
    for (const contact of contacts) {
      if (contact.id === newId) {
        return generateRandomId();
      }
    };
    return newId;
  }

  const handleSearchChange = (event) => setSearch(event.target.value);
  const handleNameChange = (event) => setName(event.target.value);
  const handleEmailChange = (event) => setEmail(event.target.value);
  const handlePhoneChange = (event) => setPhone(event.target.value);
  const handleAddressChange = (event) => setAddress(event.target.value);
  const handleWebsiteChange = (event) => setWebsite(event.target.value);
  const handleInfoChange = (event) => setInfo(event.target.value);

  const clearFields = () => {
    setName("");
    setEmail("");
    setPhone("");
    setAddress("");
    setWebsite("");
    setInfo("");
  }

  const handleAddContactButton = () => {
    setInEditingMode(false);
    setViewContactMode(false);
    setInAddingMode(true);
  }



  const handleAddContact = () => {
    const newContact = {
      id: generateRandomId(),
      name: name,
      email: email,
      phone: phone,
      address: address,
      website: website,
      info: info,
    };
    setContacts(contacts.concat(newContact));
    setInAddingMode(false);
    clearFields();
  }

  const handleEdit = () => {
    const editedContact = {
      id: contact.id,
      name: name,
      email: email,
      phone: phone,
      address: address,
      website: website,
      info: info,
    }
    const newContacts = contacts.map(c =>
      c.id !== editedContact.id
        ? c
        : editedContact);
    setContacts(newContacts);
    setInEditingMode(false);
    clearFields();
  }

  const handleCancel = () => {
    clearFields()
    setInAddingMode(false);
    setInEditingMode(false);
  }

  const handleNameClick = (event) => {
    const contact = contacts.find(c => c.id === Number(event.target.id));
    setViewContactMode(true);
    setContact(contact);
  }

  const handleRemoveContact = () => {
    setContacts(contacts.filter(c => c.id !== contact.id));
    setViewContactMode(false);
    setContact(emptyContact);
  }

  const handleEditContactButton = () => {
    setName(contact.name);
    setEmail(contact.email);
    setPhone(contact.phone);
    setAddress(contact.address);
    setWebsite(contact.website);
    setInfo(contact.info);
    setViewContactMode(false);
    setInEditingMode(true);
  }

  const Contacts = () => {
    if (search !== "") {
      const filteredContacts = contacts.filter(c => c.name.toLowerCase().includes(search.toLowerCase()));
      return (
        <ul>
          {filteredContacts.map(c => <li key={c.id} id={c.id} onClick={handleNameClick}>{c.name}</li>)}
        </ul>
      )
    }
    return (
      <ul>
        {contacts.map(c => <li key={c.id} id={c.id} onClick={handleNameClick}>{c.name}</li>)}
      </ul>
    )
  }

  const Contact = () => {
    return (
      <div>
        {
          contact.name !== ""
          && <h1>{contact.name}</h1>
        }
        {
          contact.email !== ""
          && <p>Email: {contact.email}</p>
        }
        {
          contact.phone !== ""
          && <p>Phone: {contact.phone}</p>
        }
        {
          contact.address !== ""
          && <p>Address: {contact.address}</p>
        }
        {
          contact.website !== ""
          && <p>Website: {contact.website}</p>
        }
        {
          contact.info !== ""
          && <p>Notes: {contact.info}</p>
        }
        <button onClick={handleEditContactButton}>Edit</button>
        <button onClick={handleRemoveContact}>Remove</button>
      </div>
    )
  }

  return (
    <div className="row">
      <div className="column">
        <input id="search" value={search} onChange={handleSearchChange} placeholder="search" />
        <Contacts />
        <button onClick={handleAddContactButton}>Add Contact</button>
      </div>
      <div className="column">
        {
          (!inAddingMode && !inEditingMode && !inViewContactMode)
          && <h1>The Great Contact Application</h1>
        }
        {
          inEditingMode
          && <h1>Edit Contact</h1>
        }
        {
          inAddingMode
          && <h1>Add a contact</h1>
        }
        {
          (inAddingMode || inEditingMode)
          && <div>
            <label htmlFor="name">Name: </label>
            <input type="text" id="Name" value={name} onChange={handleNameChange} />

            <label htmlFor="email">Email: </label>
            <input type="text" id="email" value={email} onChange={handleEmailChange} />

            <label htmlFor="phone">Phone: </label>
            <input type="text" id="phone" value={phone} onChange={handlePhoneChange} />

            <label htmlFor="address">Address: </label>
            <input type="text" id="address" value={address} onChange={handleAddressChange} />

            <label htmlFor="website">Website: </label>
            <input type="text" id="website" value={website} onChange={handleWebsiteChange} />

            <label htmlFor="info">Info: </label>
            <input type="text" id="info" value={info} onChange={handleInfoChange} />
          </div>
        }
        {
          inAddingMode
          && <div>
            <button onClick={handleAddContact}>Add</button>
            <button onClick={handleCancel}>Cancel</button>
          </div>
        }
        {
          inEditingMode
          && <div>
            <button onClick={handleEdit}>Save</button>
            <button onClick={handleCancel}>Cancel</button>
          </div>
        }
        {
          inViewContactMode
          && <Contact />
        }
      </div>
    </div>
  )
}

export default App
