import { useEffect } from 'react'
import { useState } from 'react'

function App() {
  const [time, setTime] = useState(0);

  useEffect(() => {
    const timeOut = setTimeout(() => {
      setTime(time + 1);
    }, 1000)

    return () => clearTimeout(timeOut);

  }, [time])

  const reset = () => setTime(0);

  return (
    <div>
      <h1>{time}</h1>
      <button onClick={reset}>Reset</button>
    </div>
  )
}

export default App
