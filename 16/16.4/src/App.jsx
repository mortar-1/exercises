import { useState } from 'react'

function App() {
  const [text, setText] = useState("");
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [feedbackType, setFeedBackType] = useState("");

  const onSubmit = (event) => {
    event.preventDefault();
    console.log(
      `
      By: ${name}
      email: ${email}
      ${feedbackType}:
      ${text}
      `
    )
    clearAll();
  }

  const clearAll = () => {
    setText("");
    setName("");
    setEmail("");
    setFeedBackType("");
  }

  const handleTextChange = (event) => setText(event.target.value);
  const handleNameChange = (event) => setName(event.target.value);
  const handleEmailChange = (event) => setEmail(event.target.value);
  const handleRadioButton = (event) => setFeedBackType(event.currentTarget.value);

  const RadioButton = ({ value }) => {
    return (
      <div>
        <label htmlFor={value}>{value}</label>
        <input
          id={value}
          type="radio"
          name="feedbackType"
          value={value}
          checked={feedbackType === value}
          onChange={handleRadioButton}
        />
      </div>
    )
  }

  return (
    <div>
      <form onSubmit={onSubmit}>
        {feedbackType === "" && <p>Feedback type must be chosen</p>}
        <RadioButton value="feedback" />
        <RadioButton value="suggestion" />
        <RadioButton value="question" />
        <textarea
          onChange={handleTextChange}
          name="text"
          cols="30"
          rows="10"
          value={text}
          placeholder={feedbackType === "" ? "feedback" : feedbackType}>
        </textarea>
        <input onChange={handleNameChange} type="text" value={name} placeholder="name" />
        <input onChange={handleEmailChange} type="text" value={email} placeholder="email" />
        <input type="submit" value="Send" disabled={feedbackType === ""} />
      </form>
      <button onClick={clearAll} >Reset</button>
    </div>
  )
}

export default App
