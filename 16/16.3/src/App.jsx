import { useState } from 'react'

function App() {
  const [numbers, setNumbers] = useState([])

  const addNumber = () => {
    const num = Math.floor(Math.random() * 42) + 1;
    if (numbers.includes(num)) {
      return addNumber();
    }
    if (numbers.length > 40) {
      return;
    }
    setNumbers(numbers.concat(num));
  }

  const Numbers = () => {
    return (
      numbers.map((n, i) => <p key ={"number" + i}>{n}</p>)
    )
  }

  const reset = () => setNumbers([]);

  return (
    <div>
     <button onClick={addNumber}>+</button>
     <button onClick={reset}>reset</button>
     <Numbers/>
    </div>
  )
}

export default App
