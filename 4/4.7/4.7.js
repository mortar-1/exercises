// eslint-disable-next-line no-unused-vars
const [a, b, ...names] = process.argv;

console.log("names:", names);

function like (names) {
    if (names.length > 3) {
        console.log(`${names[0]}, ${names[1]} and ${names.length - 2 } others`);
    } else if (names.length === 3) {
        console.log(`${names[0]}, ${names[1]} and ${names[2]} likes this`);
    } else if (names.length === 2) {
        console.log(`${names[0]} and ${names[1]} likes this`);
    } else if (names.length === 1) {
        console.log(`${names[0]} likes this`);
    } else {
        console.log("No one likes this");
    }
}

like(names);