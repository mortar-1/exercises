import fs from "fs";
const temperature = parseInt(process.argv[2]);

try{
    const data = fs.readFileSync("forecast_data.json", "utf-8");
    const forecast = JSON.parse(data);
    console.log("forecast:", forecast);
    forecast.temperature = temperature;
    console.log("new forecast:", forecast);
    const json = JSON.stringify(forecast, null, 4);
    fs.writeFileSync("forecast_data2.json", json, "utf-8");
} catch (error) {
    console.log("error:", error);
}

