// eslint-disable-next-line no-unused-vars
const [a, b, firstName, lastName] = process.argv;
const year = new Date().getFullYear();
const yearAsArray = year.toString().split("")

const generateUsername = (firstName, lastName) => {
    let username = "B";
    username += yearAsArray[yearAsArray.length - 2];
    username += yearAsArray[yearAsArray.length - 1];
    username += lastName[0].toLowerCase();
    username += lastName[1].toLowerCase();
    username += firstName[0].toLowerCase();
    username += firstName[1].toLowerCase();
    return username;
};

const generateNumberForLetter = () => {
    const a = Math.floor(Math.random() * 25) + 65;
    const b = Math.floor(Math.random() * 25) + 97;
    const c = Math.floor(Math.random() * 2);
    if (c === 0) {
        return a;
    } else {
        return b;
    }
};

const generateNumberForSpecialCharacter = () => {
    const number = Math.floor(Math.random() * 14) + 33;
    console.log("special character:", number);
    return number;
};


const generatePassword = (firstName, lastName) => {
    let password = String.fromCharCode(generateNumberForLetter());
    password += firstName[0].toLowerCase();
    password += lastName[lastName.length - 1].toUpperCase();
    password += String.fromCharCode(generateNumberForSpecialCharacter());
    password += yearAsArray[yearAsArray.length - 2];
    password += yearAsArray[yearAsArray.length - 1];
    return password;
};

const generateCredentials = (firstName, lastName) => {
    const username = generateUsername(firstName, lastName);
    const password = generatePassword(firstName, lastName);
    return [username, password];
};

console.log(generateCredentials(firstName, lastName));