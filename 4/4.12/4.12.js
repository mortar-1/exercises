let number = parseInt(process.argv[2]);


const collaz = (number) => {
    if (typeof (number) === "number") {
        let steps = 0;
        while (number !== 1) {
            steps++;
            if (number % 2 === 0) {
                number /= 2;
            } else {
                number = number * 3 + 1;
            }
        }
        return steps;
    } else {
        return "not a number";
    }
};

console.log(collaz(number));