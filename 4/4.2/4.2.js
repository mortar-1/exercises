function Ingredient(name, amount) {
    this.name = name;
    this.amount = amount;
}

Ingredient.prototype.toString = function() {
    return `name: ${this.name}, amount: ${this.amount}`;
};

Ingredient.prototype.scale = function(factor) {
    this.amount *= factor;
};

function Recipe(name, servings, ingredients = []) {
    this.name = name;
    this.ingredients = ingredients;
    this.servings = servings;
}

Recipe.prototype.toString = function () {
    return `name: ${this.name}, servings: ${this.servings}\ningredients: ${this.ingredients.map(i => i.toString()).join("\n")}`;
};

Recipe.prototype.setServings = function(newServings) {
    const scale = newServings / this.servings;
    // console.log("scale", scale);
    for (const ingredient of this.ingredients) {
        ingredient.scale(scale);
    }
    this.servings = newServings;
};

const sipuli = new Ingredient("sipuli", 1);
const tomaatti = new Ingredient("tomaatti", 2);

const resepti = new Recipe("sipulilautanen" , 1, [sipuli, tomaatti]);
console.log("resepti", resepti.toString());

// scaling
resepti.setServings(1000);
console.log(resepti.toString());

