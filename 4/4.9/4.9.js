const str = process.argv[2];

const getWovelCount = (string) => {
    if (string !== undefined) {
        const vowels = ["a", "e", "i", "o", "u", "y", "ä", "ö"];
        let numberOfVowels = 0;
        const letters = string.split("");
        console.log("letters:", letters);
        letters.forEach(letter => {
            vowels.forEach(vowel => {
                if (letter.toLowerCase() === vowel) {
                    numberOfVowels++;
                }
            });
        });
        return `number of vowels: ${numberOfVowels}`;
    } else {
        return "give a string";
    }
};

console.log(getWovelCount(str));

