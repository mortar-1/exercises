// eslint-disable-next-line no-unused-vars
let [a, b, correct, given] = process.argv;

const checkExam = (correctAnswers, givenAnswers) => {
    if (correct !== undefined && given !== undefined) {
        correct = correct.split("");
        given = given.split("");

        console.log("Correct asnwers:", correctAnswers);
        console.log("Given answers:", givenAnswers);
        if (correctAnswers.length > 0 && correctAnswers.length === givenAnswers.length) {
            let points = 0;
            for (let i = 0; i < correctAnswers.length; i++) {
                if (correctAnswers[i] === givenAnswers[i]) {
                    points += 4;
                }
                if (correctAnswers[i] !== givenAnswers[i] && givenAnswers[i] !== " ") {
                    points--;
                }
            }
            return `Points: ${points}`;
        }
    } else {
        return "Wrong input!";
    }
};


console.log(checkExam(correct, given));