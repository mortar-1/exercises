function Ingredient(name, amount) {
    this.name = name;
    this.amount = amount;
    this.toString = function () {
        return `name: ${this.name}, amount: ${this.amount}`;
    };
}


function Recipe(name, servings, ingredients = []) {
    this.name = name;
    this.ingredients = ingredients;
    this.servings = servings;
    this.toString = function () {
        return `name: ${this.name}, servings: ${this.servings}, ingredients: ${this.ingredients.map(i => i.toString()).join(",")}`;
    };
}


const sipuli = new Ingredient("sipuli", 1);


console.log("sipuli:", sipuli.toString());

sipuli.amount = 2;

console.log("uusiSipuli:", sipuli.toString());

const resepti = new Recipe("sipulilautanen" , 1, [sipuli, sipuli]);
console.log("resepti", resepti.toString());
