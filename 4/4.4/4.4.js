import fs from "fs";

fs.readFile("./joulu.txt", "utf-8", (err, file) => {
    if (err) {
        console.log("Error:", err);
    } else {
        file = file.replaceAll("Joulu", "Kinkku");
        file = file.replaceAll("joulu", "kinkku");
        file = file.replaceAll("Lapsilla", "Poroilla");
        file = file.replaceAll("lapsilla", "poroilla");
        fs.writeFile("./joulu2.txt", file, (err) => {
            if (err) {
                console.log("Error:", err);
            } else {
                console.log(file);
            }
        });
    }
});