const getValue = function () {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve({ value: Math.random() })
        }, Math.random() * 1500)
    })
}


const printWithAwait = async () => {
    const value1 = await getValue();
    const value2 = await getValue();
    console.log(`Value1 = ${value1.value}, value2 = ${value2.value}`);
}

const printWithThen = () => {
    let value1;
    getValue()
        .then(result => {
            value1 = result.value;
            return getValue()
                .then(resutl => console.log(`value1 = ${value1.value}, value2 = ${result.value}`))
        })
}

printWithAwait();
printWithAwait();