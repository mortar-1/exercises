console.log("3");
new Promise( (resolve, reject) => {
    setTimeout(() => {
        console.log("..2");
        resolve();
    },1000)
})
.then( () => {
    return new Promise( (resolve, reject) => {
        setTimeout( () => {
            console.log("....1");
            resolve();
        }, 1000)
    })
})
.then( () => {
    return new Promise ( (resolve, reject) => {
        setTimeout(() => {
            console.log("GO!");
            resolve();
        }, 1000)
    })
})
