const elements = {
    name: document.getElementById("name"),
    year: document.getElementById("year"),
    button: document.getElementById("fetch")
}

elements.button.addEventListener("click", (event) => {
    event.preventDefault();
    const name = elements.name.value;
    const year = elements.year.value;
    console.log("name", name);
    console.log("year", year);
    axios.get(`https://www.omdbapi.com/?apikey=c3a0092f&t=${name}&y=${year}`)
        .then(response => {
            console.log(response.data);
            const title = document.createElement("li");
            const year = document.createElement("li")
            const imdb = document.createElement("li")
            title.innerHTML = response.data.Title;
            year.innerHTML = response.data.Year;
            imdb.innerHTML = `https://www.imdb.com/title/${response.data.imdbID}/`;
            document.getElementById("list").appendChild(title);
            document.getElementById("list").appendChild(year);
            document.getElementById("list").appendChild(imdb);
        })
        .catch(error => console.error(error))
})


