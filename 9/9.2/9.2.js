import ekspressi from "express";

const serveri = ekspressi();

serveri.listen(3000, () => {
    console.log("Serveri kuuntelee porttia 3000");
});

serveri.get("/", (req, res) => {
    res.send("Hello World")
});

serveri.get("/endpoint2", (req, res) => {
        res.json({response: "Hattivatti"});
})