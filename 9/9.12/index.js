import express from "express";
import midWare from "./utils/middleware.js";
import booksRouter from "./controllers/books.js";

const app = express();
app.use(midWare.logger);
app.use(express.static("public"));
app.use(express.json());
app.use(midWare.checkParamsForPost);
app.use(midWare.checkParamsForPut);

app.get("/", (req, res) => {
    console.log("foaksndfkjladsnflkjfsd");
    res.send("index");
})

app.use("/api/v1/books", booksRouter);

app.use(midWare.unknownEndpoint);

const PORT = process.env.port || 3000;
app.listen(PORT, () => {
    console.log(`Listening to port ${PORT}`);
})

