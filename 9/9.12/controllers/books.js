import express from "express";
import idHandler from "../utils/idHandler.js";

const booksRouter = express.Router();

let books = [];

booksRouter.get("/", (req, res) => {
    res.send(books);
})

booksRouter.get("/:id", (req, res) => {
    const id = idHandler.id(req);
    const book = idHandler.findItemById(id, books);
    if (book) {
        res.status(200).json(book);
    } else {
        res.status(404).send({ error: "id not existing" });
    }

})

booksRouter.post("/", async (req, res) => {
    let { name, author, read } = req.body;
      let id = idHandler.generateRandomId(books);
    read = read ? true : false;
    let book = { id, name, author, read };
    books.push(book);
    res.status(200).end();
})

booksRouter.put("/:id", async (req, res) => {
    const id = idHandler.id(req);
    const { name, author, read } = req?.body;
    if (idHandler.idExists(id, books)) {
        let book = await idHandler.findItemById(id, books);
        console.log("book:", book);
        book.name = name ? name : book.name;
        book.author = author ? author : book.author;
        book.read = (read !== undefined || read) ? true : book.read;
        res.status(204).json(book);
    } else {
        res.status(404).send({ error: "no item with such id" });
    }
})

booksRouter.delete("/:id", async (req, res) => {
    const book = await idHandler.findItemByReq(req, books);
    if (book) {
        books = books.filter(b => b.id !== book.id);
        res.status(204).send({ whatHappened: `deletion of ${book.name} by ${book.author} succeeded` });
    } else {
        res.status(404).send({ error: "no item with such id" });
    }
})

export default booksRouter;