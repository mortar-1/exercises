const logger = async (req, res, next) => {
    console.log("--------------------------------------------");
    console.log("url:", req.path);
    console.log("method:", req.method);
    console.log("Time:", new Date().toString());
    console.log("Body:", await req.body);
    console.log("--------------------------------------------");
    next();
}

const checkParamsForPost = async (req, res, next) => {
    const method = await req.method;
    let { name, author } = req.body;
    if (method === "POST" && (!name || !author)) {
        res.status(400).send({ error: "name and author needed!!!" })
    }
    next();
}

const checkParamsForPut = async (req, res, next) => {
    const method = await req.method;
    let { name, author, read } = req.body;
    if (method === "PUT" && (!name && !author && !read)) {
        res.status(400).send({ error: "parameters name, author or read needed!!!" });
    }
    next();
}

const unknownEndpoint = (req, res, next) => {
    res.status(404).send({ error: "There's nothing to look at here!" });
}

export default { logger, unknownEndpoint, checkParamsForPost, checkParamsForPut };