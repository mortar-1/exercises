const id = (req) => {
    return Number(req.params?.id);
}

const idExists = (id, listOfObjects) => {
    return listOfObjects.find(item => item.id === id) ? true : false;
}

const findItemByReq = (req, listOfObjects) => {
    return findItemById(id(req), listOfObjects);
}

const findItemById = (id, listOfObjects) => {
    return listOfObjects.find(item => item.id === id);
}

const generateRandomId = (listOfObjects) => {
    let id = Math.floor(1000 + Math.random() * 9000)
    if (idExists(id, listOfObjects)) {
        generateRandomId(listOfObjects);
    } else {
        return id;
    }
}

export default { id, generateRandomId, idExists, findItemById, findItemByReq };
