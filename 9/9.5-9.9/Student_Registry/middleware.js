
const requestLogger = async (req, res, next) => {
    console.log("method:", req.method);
    console.log("path:", req.path);
    console.log("timeStamp:", new Date().toString());
    console.log("body:", await req.body);
    next();
}

const unknownEndpoint = (req, res, next) => {
    res.status(404).send({ error: "No one here" });
}

export default {requestLogger, unknownEndpoint};