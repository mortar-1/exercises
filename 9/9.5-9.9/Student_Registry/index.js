import express, { urlencoded } from "express";
import middleware from "./middleware.js";

let students = [];

const app = express();
app.use(express.static("public"))
app.use(express.json());
app.use(urlencoded({ extended: false }));
app.use(middleware.requestLogger);

app.get("/", (req, res) => {
    res.send("index")
})

app.get("/students", async (req, res) => {
    res.json(students.map(s => s.id));
});

app.get("/students/:id", (req, res) => {
    const id = Number(req.params.id);
    const student = students.find(s => s.id === id);
    if (!student) {
        res.status(404).end();
    } else {
        res.json(student);
    }
})

app.post("/student", async (req, res) => {
    const body = await req.body;
    if (!body.id || !body.name || !body.email) {
        res.status(400).json({ error: "name, id or email missing" })
    } else {
        const student = { name: body.name, email: body.email, id: body.id };
        students.push(student);
        res.status(201).end();
    }
});

app.put("/student/:id", async (req, res) => {
    const id = Number(req.params.id);
    const body = await req.body;
    if (!body.email && !body.name) {
        res.status(400).send({ error: "name or email missing" })
    } else {
        const student = students.find(s => s.id === id);
        if (!student) {
            res.status(404).end();
        } else {
            student["name"] = body.name ? body.name : student["name"];
            student["email"] = body.email ? body.email : student["email"];
            res.status(204).end()
        }
    }
})

app.delete("/student/:id", (req, res) => {
    const id = Number(req.params.id);
    const student = students.find(s => s.id === id);
    if (!student) {
        res.status(404).end()
    } else {
        students = students.filter(s => s.id !== id);
        res.status(204).end();
    }
})

app.use(middleware.unknownEndpoint);

app.listen(3000);
console.log("Server running on port 3000");
