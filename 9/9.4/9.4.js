import expres from "express";

const server = expres();

let count = 0;

const baseUrl = "/counter";

server.get(baseUrl, (req, res) => {
    if (req.query.number !== undefined) {
        count = req.query.number;
    } else {
        count++;
    }
    res.send(`<h1>count is ${count}</h1>`);
});

const visitors = {};
server.get(`${baseUrl}/:name`, (req, res) => {
    let name = req.params.name;
    visitors[name] === undefined ? visitors[name] = 1 : visitors[name]++;
    res.send(`<h1>${name} was here ${visitors[name]} times </h1>`);
})

server.listen(3000);
console.log("listening 3000");