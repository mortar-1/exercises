import express from "express"

const server = express()

let count = 0;

server.get("/counter", (req, res) => {
    if (req.query.number !== undefined) {
        count = req.query.number;
    } else {
        count++;
    }
    res.send(`<h1>count is ${count}</h1>`);
})

server.listen(3000);
console.log("listening 3000");