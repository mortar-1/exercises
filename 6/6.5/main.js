const button = document.getElementById("button");

button.addEventListener("click", () => {
    
    document.getElementById("results").replaceChildren();

    const textArea = document.getElementById("text");
    let text = textArea.value;
    let uniqueWords = {};
    let sortedUniqueWords = [];
    let wordCount = 0;
    let wordLengthSum = 0;
    let averageWordLength = 0;
    const words = text.replace(/\n/g, " ").split(" ")

    let specialCharacters = "[`!@#$€%^&*()_+-=[]{};':\"\\|“”,.<>/?~]/"
    specialCharacters = specialCharacters.split("");

    const endsWithSpecialCharacter = (word) => {
        for (const char of specialCharacters) {
            if (char === word[word.length - 1]) {
                return true;
            }
        }
        return false;
    }

    const startsWithSpecialCharacter = (word) => {
        for (let char of specialCharacters) {
            if (word[0] === char) {
                return true;
            }
        }
        return false;
    }

    words.forEach(word => {
        // Erikoismerkit tarkistetaan neljästi esimerkiksi tällaisen kohdan takia: 'kunnes hän "herää"...'
        for (let i = 0; i < 4; i++) {
            if (endsWithSpecialCharacter(word)) {
                word = word.substring(0, word.length - 1);
            }
        }
        for (let i = 0; i < 4; i++) {
            if (startsWithSpecialCharacter(word)) {
                word = word.substring(1, word.length);
            }
        }

        word = word.toLowerCase();

        if (word.length > 1) {
            if (uniqueWords[word]) {
                uniqueWords[word] = uniqueWords[word] + 1;
            } else {
                uniqueWords[word] = 1;
            }
            wordCount++;
            wordLengthSum += word.length;
        }
    })

    for (let wrd in uniqueWords) {
        sortedUniqueWords.push([wrd, uniqueWords[wrd]])
    }
    sortedUniqueWords.sort((a, b) => {
        return b[1] - a[1];
    })

    averageWordLength = wordLengthSum / wordCount;

    console.log("uniquieWords:", uniqueWords);
    console.log("wordCount:", wordCount);
    console.log("wordLengthSum:", wordLengthSum);
    console.log("averageWordLength:", averageWordLength);
    sortedUniqueWords.forEach(item => console.log(`${item[0]}: ${item[1]} times`));

    const woddCountTag = document.createElement("p");
    const averageLengthTag = document.createElement("p");
    woddCountTag.innerHTML = `Word count: ${wordCount}`;
    averageLengthTag.innerHTML = `Average word length: ${averageWordLength}`;
    document.getElementById("results").appendChild(woddCountTag);
    document.getElementById("results").appendChild(averageLengthTag);
    const header = document.createElement("h1");
    header.innerHTML = "Words and their occurance:"
    document.getElementById("results").appendChild(header);
    sortedUniqueWords.forEach(item => {
        const uniqueWordTag = document.createElement("p");
        uniqueWordTag.innerHTML = `${item[0]}: ${item[1]} times.`
        document.getElementById("results").appendChild(uniqueWordTag);
    })
})
