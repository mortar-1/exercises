const inputfield = document.getElementById("input");
const removeBtn = document.getElementById("remove");

const addDoability = (element) => {
    element.addEventListener('click', () => {
        if (element.style.color === "gray") {
            element.removeAttribute("style");
            element.classList.remove("done");
        } else {
            element.style.color = "gray";
            element.style.textDecoration = "line-through";
            element.classList.add("done");
        }
    })
}

const addTask = () => {
    const taskElement = document.createElement("li");
    taskElement.innerHTML = inputfield.value;
    addDoability(taskElement);
    document.getElementById("list").appendChild(taskElement);
    inputfield.value = "";
}

inputfield.addEventListener('keypress', (event) => {
    if (event.key === "Enter") {
        addTask();
    }
})

removeBtn.addEventListener('click', () => {
    document.querySelectorAll(".done").forEach(el => el.remove());
})
