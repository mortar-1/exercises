let mark = "X";

const changeMark = () => {
    mark = mark === "X" ? "O" : "X";
}

const isEmpty = (element) => {
    return element.innerHTML === "";
}

document.querySelectorAll(".button")
    .forEach((box, i) => {
        box.addEventListener("click", () => {
            if (isEmpty(box)) {
                box.innerHTML = mark;
                changeMark();
            }
        })
    })
