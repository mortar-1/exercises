const div = document.getElementById("div");
const originalParagraph = document.getElementById("original");

const sentences = originalParagraph.innerHTML.split(".");

const paragraphs = [];

sentences.forEach(sentence => {
    sentence = sentence.trim();
    const pTag = document.createElement("p");
    sentence.split(" ").forEach(word => {
        if (word.length > 5) {
            pTag.innerHTML += `<span style="color:yellow;">${word}</span> `
        } else {
            pTag.innerHTML += `${word} `;
        }
    })
    paragraphs.push(pTag);
})

div.removeChild(originalParagraph);

paragraphs.forEach(s => document.getElementById("div").appendChild(s));

