
const generateLotteryNumbers = () => {
    const lotteryNumbers = [];
    while (lotteryNumbers.length < 7) {
        let randomNumber = Math.floor(Math.random() * 40) + 1;
        if (!lotteryNumbers.includes(randomNumber)) {
            lotteryNumbers.push(randomNumber);
        }
    }
    return lotteryNumbers;
};

const logWithOneSecDelay = (array) => {
    let milliseconds = 0;
    for (let i = 0; i < array.length; i++) {
        setTimeout(() => {
            console.log(array[i]);
        }, milliseconds);
        milliseconds += 1000;
    }
};

logWithOneSecDelay(generateLotteryNumbers());