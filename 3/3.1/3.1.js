const str = process.argv[2];

const firstLetterToUpperCase = (str) => {
    let words = str.split(" ");
    console.log("words:", words);
    for (let i = 0; i < words.length; i++) {
        const firstLetter = words[i][0].toUpperCase();
        const restOfTheWord = words[i].substring(1);
        const theWholeWord = firstLetter + restOfTheWord;
        words[i] = theWholeWord;
    }
    console.log(words);
    str = "";
    words.forEach(word => {
        str += word;
        str += " ";
    });
    return str;
};

console.log(firstLetterToUpperCase(str));

