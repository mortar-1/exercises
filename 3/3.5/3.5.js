const str = process.argv[2];
const arrayFromString = str.split("");


for (let i = 1; i < arrayFromString.length; i++) {
    if (arrayFromString[i] !== arrayFromString[i - 1] && arrayFromString[i] !== arrayFromString[i + 1]) {
        console.log("First non-repeating:", arrayFromString[i]);
        break;
    }
}
