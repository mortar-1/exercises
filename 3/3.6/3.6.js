// 1:
// const mergeArrays = (arr1, arr2) => {
//     let mergedArray = [];
//     arr1.forEach(element => {
//         if(!mergedArray.includes(element)){
//             mergedArray.push(element);
//         }
//     });
//     arr2.forEach(element => {
//         if(!mergedArray.includes(element)) {
//             mergedArray.push(element);
//         }
//     });
//     return mergedArray;
// };

// 2:
// const mergeArrays = (arr1, arr2) => {
//     const mergedArray = arr1.concat(arr2);
//     const uniqueElements = mergedArray.filter((element, index) => {
//         console.log("element:", element);
//         console.log("index:", index);
//         return mergedArray.indexOf(element) === index;
//     });
//     return uniqueElements;
// };

// 3:
const mergeArrays = (arr1, arr2) => {
    let mergedArray = arr1.concat(arr2);
    mergedArray = [...new Set(mergedArray)]; 
    return mergedArray;
};

const arr1 = [1,2,3,4,4];
const arr2 = [3,4,5,6,6];

console.log(mergeArrays(arr1, arr2));