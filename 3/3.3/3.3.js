const number = parseInt(process.argv[2]);
let factoral = number;

const factoralFunction = (number) => {
    if (number > 1) {
        factoral *= (number - 1);
        return factoralFunction(number - 1);
    } else {
        return factoral;
    }
};

console.log(factoralFunction(number));