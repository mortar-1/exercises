const word = process.argv[2];

const reverseWord = (word) => {
    let letters = word.split("");
    letters = letters.reverse();
    let reverseStr = letters.join("");
    // let reverseStr = "";
    // letters.forEach(letter => reverseStr += letter);
    return reverseStr;
    // You can use array method .join() to make an array into a string
};

console.log(reverseWord(word));
