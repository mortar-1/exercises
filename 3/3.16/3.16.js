const word = process.argv[2];

const reverseWord = (word) => {
    let letters = word.split("");
    let reverse = letters.reverse();
    return reverse.join("");
};

const isPalindrome = (word) => {
    const reverseStr = reverseWord(word);
    return reverseStr.toLowerCase() === word.toLowerCase();
};

console.log(isPalindrome(word));


// if (reverseStr.toLowerCase() === word.toLowerCase()) {
//     return true;
// } else {
//     return false;
// }
// Tämä kannattaa ilmaista yksinkertaisemmin:
// return reverseStr.toLowerCase() === word.toLowerCase()