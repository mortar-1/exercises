const array = [2, 4, 5, 6, 8, 10, 14, 18, 25, 32];

const randomizer = (array) => {
    let randomizedArray = [];
    let usedIndexes = [];
    let randomIndex = Math.floor(Math.random() * array.length);
    while (randomizedArray.length < array.length) {
        if (!usedIndexes.includes(randomIndex)) {
            usedIndexes.push(randomIndex);
            randomizedArray.push(array[randomIndex]);
        } else {
            randomIndex = Math.floor(Math.random() * array.length);
        }
    }
    return randomizedArray;
};

console.log(randomizer(array));