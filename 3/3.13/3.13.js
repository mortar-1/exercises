const competitors = ["Julia", "Mark", "Spencer", "Ann", "John", "Joe"];
const ordinals = ["st", "nd", "rd", "th"];

let output = [];

for (let i = 1; i <= competitors.length; i++) {
    if (i <= 3) {
        output.push(`${i + ordinals[i-1]} competitor was ${competitors[i-1]}`);
    } else {
        output.push(`${i + ordinals[3]} competitor was ${competitors[i-1]}`);
    }
}

console.log(output);