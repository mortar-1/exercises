// eslint-disable-next-line no-unused-vars
const [a, b, num1, num2] = process.argv;

/**
 * A function that return a random integer between given values
 * @param {number} num1 lower value
 * @param {number} num2 higher value
 * @returns 
 */

const randomNumber = (num1, num2) => {
    return Math.floor(Math.random() * (num2 - num1) + num1);
};

console.log(randomNumber(num1, num2));