const word = process.argv[2];

const turnInToIndex = (word) => {
    if (word !== undefined) {
        const charIndex = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "x", "y", "z"];
        const letters = word.split("");
        let str = "";
        letters.forEach(letter => str += (charIndex.indexOf(letter) + 1));
        return str;
    }
};

console.log(turnInToIndex(word));
