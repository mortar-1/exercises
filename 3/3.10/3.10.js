const bearing = Number(process.argv[2]);

const determineRunway = (bearing) => {

    if (bearing <= 360 || bearing > 0) {

        if (bearing < 15) {
            return 10;
        }

        let rounded = bearing - bearing % 10;
        if (bearing % 10 >= 5) {
            rounded += 10;
        }
        return rounded / 10;
    } else {
        return "faulted bearing";
    }
};

console.log(determineRunway(bearing));