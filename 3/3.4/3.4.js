const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22];

// 1:
const divisibleByThree = arr.filter(n => n % 3 === 0);
console.log("Divissible by three:", divisibleByThree);

//  2:

const doubled = arr.map(n => n * 2);
console.log("Doubled:", doubled);

//  3:

const reducer = (sum, item) => {
    return sum += item;
};
const sumOfAll = arr.reduce(reducer, 0);
console.log("Sum of all:", sumOfAll);
