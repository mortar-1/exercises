const num = parseInt(process.argv[2]);

const generateFibonacci = (number) => {

    if (number > 0) {
        let fibonacci = [0, 1];
        if (number === 1) {
            return [0];
        } else if (number === 2) {
            return fibonacci;
        } else {
            for (let i = 2; i < number; i++) {
                fibonacci.push(fibonacci[i - 1] + fibonacci[i - 2]);
            }
            return fibonacci;
        }
    }
};

console.log(generateFibonacci(num));