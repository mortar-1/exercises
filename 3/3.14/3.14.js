const start = Number(process.argv[2]);
const end = Number(process.argv[3]);

const fromStastToEnd = (start, end) => {
    let arr = [];
    if (start < end) {
        for (let i = start; i <= end; i++) {
            arr.push(i);
        }
    } else {
        for (let i = start; i >= end; i--) {
            arr.push(i);
        }
    }
    return arr;
};

console.log(fromStastToEnd(start, end));