const distance = 150 // km
const speed = 80 // km/h

const travelTime = distance / speed

console.log('Travel time:', travelTime)
