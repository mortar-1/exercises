let price
let discount

price = 10.00
discount = 0.10 //10%

const discountedPrice = price - (price * discount)

console.log('Discounted price:', discountedPrice)
