const readline = require('readline')

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})


rl.question("Which number should \'a\' be?", (answer1) => {
    const a = parseInt(answer1)
    rl.question("Which number should \'b\' be?", (answer2) => {
        const b = parseInt(answer2)
        doTheMath(a, b)
        rl.close()
    })
})


const doTheMath = (a, b) => {
        
    const sum = a + b
    const difference = a - b
    const fraction = a / b
    const product = a * b
    const exponentiation = a ** b
    const modulo = a % b
    const average = (a + b) / 2

    console.log('a = ', a, '; b = ', b)
    console.log('sum:', sum)
    console.log('difference:', difference)
    console.log('fraction:', fraction)
    console.log('product:', product)
    console.log('exponentiation:', exponentiation)
    console.log('modulo:', modulo)
    console.log('average:', average)
}





