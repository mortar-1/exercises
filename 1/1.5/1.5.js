const readline = require('readline')

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.question("Enter a string: ", (answer) => {

    let str = answer

    /*
    str = str.trim()
    if (str.length > 20) {
        str = str.substring(0, 20)
    }
    str = str[0].toLowerCase() + str.substring(1)
    console.log('cleaned string:', str)
    rl.close()
    */
    
    const hasWhiteSpaceInBeginningOrEnd = (string) => {
        return string[0] === ' ' || string[string.length - 1] === ' '
    }

    const startsWithCapitalLetter = (string) => {
        return string[0] === string[0].toUpperCase()
    }

    const lenghtIsOver20 = (string) => {
        return string.length > 20
    }

    if (hasWhiteSpaceInBeginningOrEnd(str) || startsWithCapitalLetter(str) || lenghtIsOver20(str)) {
        console.log('error: string does not comply to given restrictions!')
    }

    rl.close()
    
})