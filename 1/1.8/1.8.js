const days = 365
const hours = 24
const minutes = 60
const seconds = 60

const secondsInYear = days * hours * minutes * seconds

console.log('Seconds in a year:', secondsInYear)
