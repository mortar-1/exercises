const readline = require('readline')

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

rl.question("Enter first strign: ", (answer1) => {
    const str1 = answer1
    rl.question("Enter second string: ", (answer2) => {
        const str2 = answer2
        analyzeStrings(str1, str2)
        rl.close()
    })
})

const analyzeStrings = (str1, str2) => {

    const logIfLenthUnderAverage = (string) => {
        const length = string.length
        if (length < str_avg) {
            console.log(`Length of strign \'${string}\' is smaller than the average length`)
            
        }
    }

    console.log('String 1:', str1)
    console.log('String 2:', str2)
    const str_sum = str1 + str2
    console.log('Combined Strign:', str_sum)
    console.log('String 1 length:', str1.length)
    console.log('String 2 length:', str2.length)
    const str_avg = (str1.length + str2.length) / 2
    console.log('Average lentg:', str_avg)
    logIfLenthUnderAverage(str1)
    logIfLenthUnderAverage(str2)
    logIfLenthUnderAverage(str_sum)
}