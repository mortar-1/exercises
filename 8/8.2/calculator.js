// const num1 = parseInt(process.argv[2]);
// const num2 = parseInt(process.argv[3]);

function calculator(operator, num1, num2) {
  
    if (operator === "+") {
        return num1 + num2;
    } else if (operator === "-") {
        return num1 - num2;
    } else if (operator === "*") {
        return num1 * num2;
    } else if (operator === "/") {
        return num1 / num2;
    } else {
        return "Can't do that.";
    }
}

// console.log(calculator("/", 5, 0));

// let operator = "+";
// console.log("Answer:", calculator(operator, num1, num2));
// operator = "-";
// console.log("Answer:", calculator(operator, num1, num2));
// operator = "*";
// console.log("Answer:", calculator(operator, num1, num2));
// operator = "/";
// console.log("Answer:", calculator(operator, num1, num2));

// export default calculator;


